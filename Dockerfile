FROM docker:20-dind

# Enable experimental feature
RUN mkdir -p /etc/docker && echo '{"experimental": true}' > /etc/docker/daemon.json

# Enable for root user as well
RUN mkdir -p /root/.docker && echo '{"experimental": "enabled"}' > /root/.docker/config.json

ENV DOCKER_CLI_EXPERIMENTAL=enabled

VOLUME /var/lib/docker
EXPOSE 2375 2376

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []
